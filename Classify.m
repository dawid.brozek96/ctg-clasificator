%test 
%podajemu tutaj sparametryzowane juz p, s , v i na tej podstawie liczymy Qi
%dla kolejnych zbior�w

function  [outTRUE, swoistosc, czulosc]  = QiTRUE(P_daszek, s, c, data, nrule, PN , FN , FP, PP)
% data=[];
% nrule=4;
% for kk1=1:276
% rann=randi([-1 1],1,1);
% if (rann == 0)
%     rann=1;
% end
% rannn=[rand(1, 12) ,  rann];
%     data = [data ;  rannn];
% end
% Plus_Minus= 2*randi([0 1],1)-1; 
% vp2=[]; 
%           for prule=1:nrule
%               vp1=[];
%             for p=1:13
%                 Plus_MinusV= 2*randi([0 1],1)-1; % +1 -1 50procent szaansy
%                 vp= (rand(1, 1) * Plus_MinusV);
%                 vp1=[vp1 ; vp];
%             end
%             vp2=[vp2 ; vp1'];
%             P_daszek=vp2;
%           end
% P_daszek=P_daszek';
% c=rand(12, nrule); 
% s=rand(12, nrule);
% PN = 0;
% FN=0;
% FP=0;
% PP=0;
[n1 m1]=size(data);
tabela_klasyfiaktora=[];
G=10E6* eye(m1*nrule); 
for n=1:n1    
    diff1=(data(n,1:m1-1)'*ones(1 ,nrule))-c ; 
    F=exp(-0.5*sum((diff1./s).^2)); 
        if (sum(F) < eps )
        F=ones(1, nrule) * eps;
        end
    F_daszek= F / sum(F); %10.17 
    D1=[data(n,1:m1-1)' ; 1] *F_daszek;
    D=D1(:);
    %P_daszek=reshape(P_daszek,[13*nrule, 1]);
    %y_spr2=sum(D .* P_daszek); %10.20
    y_spr2=D' * P_daszek; % 10.20, dlaczego musze transponowac macierz??
    tabela_klasyfiaktora = [tabela_klasyfiaktora ; y_spr2];
end

klasyfikator = tabela_klasyfiaktora;
% for i=1:n1
%     if klasyfikator(i , 1) > 0 && data(i, 13) == 1
%         PN=PN+1;
%     end
%     
%     if klasyfikator(i , 1) > 0 && data(i, 13) == -1
%         FN=FN+1;
%     end
%         
%     if klasyfikator(i , 1) <= 0 && data(i, 13) == 1
%         FP=FP+1;
%     end
%         
%     if klasyfikator(i , 1) <= 0 && data(i, 13) == -1
%         PP=PP+1;
%     end
% end

% konwersja odpowiedzi LSVM na etykiety +1 i -1
etykietyRMNK=2*(klasyfikator>0)-1;
% wskazniki prognost.
etykietyDane=data(1:n1, 13);
PP_vec=( etykietyRMNK==-1 & etykietyDane==-1 ); PP=sum(PP_vec);
PN_vec=( etykietyRMNK==1 & etykietyDane==1 );   PN=sum(PN_vec);
FP_vec=( etykietyRMNK==-1 & etykietyDane==1 );  FP=sum(FP_vec);
FN_vec=( etykietyRMNK==1 & etykietyDane==-1 );   FN=sum(FN_vec);

%-----------------------------------------        
%obliczanie Qi
swoistosc = (PN / (PN + FP)) * 100;
czulosc = (PP / (PP +FN) ) * 100;
outTRUE=sqrt(swoistosc * czulosc);
end

