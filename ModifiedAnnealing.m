clc;
clear all;
tic;



currentFolder = pwd;
Choice_reference = 'Choose a reference : 1= Ph, 2= Percentyl, 3= Apgar: ';
Case_reference = input(Choice_reference);

                        switch Case_reference
                        case 1
                        data_name='Ph';
                        load('merged data\mergedU_Ph.mat');
                        load('merged data\mergedT_Ph.mat');
                                        Choice_nrule = 'Choose a number of rules from 4 to 20: ';
                                        Case_nrule = input(Choice_nrule);
                                        nrule=Case_nrule;
                                        switch (Case_nrule/2)
                                                                case 2
                                                                load('initial permises\p_start_m11_c2_normPh')
                                                                case 3
                                                                load('initial permises\p_start_m11_c3_normPh')
                                                                case 4
                                                                load('initial permises\p_start_m11_c4_normPh')
                                                                case 5
                                                                load('initial permises\p_start_m11_c5_normPh')
                                                                case 6
                                                                load('initial permises\p_start_m11_c6_normPh')
                                                                case 7
                                                                load('initial permises\p_start_m11_c7_normPh')
                                                                case 8
                                                                load('initial permises\p_start_m11_c8_normPh')
                                                                case 9
                                                                load('initial permises\p_start_m11_c9_normPh')
                                                                case 10
                                                                load('initial permises\p_start_m11_c10_normPh')
                                        end
                        case 2
                        data_name='Percentyl';
                        load('merged data\mergedU_Percentyl.mat');
                        load('merged data\mergedT_Percentyl.mat');
                            Choice_nrule = 'Choose a even number of rules from 4 to 20: ';
                            Case_nrule = input(Choice_nrule);
                            nrule= Case_nrule;
                            switch (Case_nrule/2)
                                                    case 2
                                                    load('initial permises\p_start_m11_c2_normPercentyl')
                                                    case 3
                                                    load('initial permises\p_start_m11_c3_normPercentyl')
                                                    case 4
                                                    load('initial permises\p_start_m11_c4_normPercentyl')
                                                    case 5
                                                    load('initial permises\p_start_m11_c5_normPercentyl')
                                                    case 6
                                                    load('initial permises\p_start_m11_c6_normPercentyl')
                                                    case 7
                                                    load('initial permises\p_start_m11_c7_normPercentyl')
                                                    case 8
                                                    load('initial permises\p_start_m11_c8_normPercentyl')
                                                    case 9
                                                    load('initial permises\p_start_m11_c9_normPercentyl')
                                                    case 10
                                                    load('initial permises\p_start_m11_c10_normPercentyl')
                        end

                        case 3
                        data_name='Apgar';
                        load('merged data\mergedU_Apgar.mat');
                        load('merged data\mergedT_Apgar.mat'); 
                            Choice_nrule = 'Choose a number of rules from 4 to 20: ';
                            Case_nrule = input(Choice_nrule);
                            nrule=Case_nrule;
                            switch (Case_nrule/2)
                                                    case 2
                                                    load('initial permises\p_start_m11_c2_normApgar')
                                                    case 3
                                                    load('initial permises\p_start_m11_c3_normApgar')
                                                    case 4
                                                    load('initial permises\p_start_m11_c4_normApgar')
                                                    case 5
                                                    load('initial permises\p_start_m11_c5_normApgar')
                                                    case 6
                                                    load('initial permises\p_start_m11_c6_normApgar')
                                                    case 7
                                                    load('initial permises\p_start_m11_c7_normApgar')
                                                    case 8
                                                    load('initial permises\p_start_m11_c8_normApgar')
                                                    case 9
                                                    load('initial permises\p_start_m11_c9_normApgar')
                                                    case 10
                                                    load('initial permises\p_start_m11_c10_normApgar')
                            end

                        end





a=0.99; % cooling factor: choose between: 0.8 - 0.99
T=10E15;% initial temperature: recommended between 1 - 10E16


%@@@@@@@@@@@@@@@ uncoment it if you start with early prepared initial conclusions
v1 = [V1; V2]';
s1 = [S1; S2]';
%@@@@@@@@@@@@@@@

%@@@@@@@@@@@@@@@@ uncoment it if you start with random initial conclusion,
%TODO option to choose prepared / random
% v1=rand(12, nrule); 
% s1=rand(12, nrule);
%           ind=v1<0;
%           v1(ind)=0;
%           ind=v1>1;
%           v1(ind)=1;
%           ind=s1<=0;
%           s1(ind)=eps;
%@@@@@@@@@@@@@@@@


Table_Qi_first=[];
for kl=1:5
                        switch kl
                        case 1
                            dataKu=mergedU(1:275, 1:13);
                            dataKt=mergedT(1:276, 1:13);
                        case 2
                            dataKu=mergedU(276:550, 1:13);
                            dataKt=mergedT(277:552, 1:13);
                        case 3
                            dataKu=mergedU(551:825, 1:13);
                            dataKt=mergedT(553:828, 1:13);
                        case 4
                            dataKu=mergedU(826:1100, 1:13);
                            dataKt=mergedT(829:1104, 1:13);
                        case 5
                            dataKu=mergedU(1101:1375, 1:13);
                            dataKt=mergedT(1105:1380, 1:13);
                        end

First_conclusion_parameters  = Conclusion(dataKu , v1, s1, 1, nrule);
Qi_first  = Classify(First_conclusion_parameters, s1, v1, dataKt, nrule, 0, 0, 0, 0);
Initial_Table_Best_parameters={Qi_first, First_conclusion_parameters, v1, s1};
Table_Qi_first=[Table_Qi_first, Qi_first];
end
Qi_first=mean(Table_Qi_first)
Table_Best_parameters={Qi_first, v1, s1};



s_best=s1; 
v_best=v1; 



v=v1;
s=s1;
Era=0;

% simulated annealing
 for L = 1 : 2 %number of era

        
        
        
		while true
          v_initial2=randn(12, nrule);
          s_initial2=randn(12, nrule);
          v_initial= v + v_initial2; % neighborhood matrix v
          s_initial= s + s_initial2; % neighborhood matrix s
          
          %cutting 0<v<1 , 0<s
          ind=v_initial<0;
          v_initial(ind)=0;
          ind=v_initial>1;
          v_initial(ind)=1;
          ind=s_initial<=0;
          s_initial(ind)=eps;
         
                 Table_Qi_Annealing=[];
                 for kl=1:5
                        switch kl
                        case 1
                            dataKu=mergedU(1:275, 1:13);
                            dataKt=mergedT(1:276, 1:13);
                        case 2
                            dataKu=mergedU(276:550, 1:13);
                            dataKt=mergedT(277:552, 1:13);
                        case 3
                            dataKu=mergedU(551:825, 1:13);
                            dataKt=mergedT(553:828, 1:13);
                        case 4
                            dataKu=mergedU(826:1100, 1:13);
                            dataKt=mergedT(829:1104, 1:13);
                        case 5
                            dataKu=mergedU(1101:1375, 1:13);
                            dataKt=mergedT(1105:1380, 1:13);
                        end
                
                        Annealing_conclusion_parameters  = Conclusion(dataKu , v_initial, s_initial, 1, nrule); %tutaj mi sie zmienia te p - dzieki NM
                        Qi_Annealing  = Classify(Annealing_conclusion_parameters, s_initial, v_initial, dataKt, nrule, 0, 0, 0, 0); 
                        Table_Qi_Annealing=[Table_Qi_Annealing, Qi_Annealing]; %dla testowych licze Qi
                        
                    end
                         Qi_Annealing=mean(Table_Qi_Annealing);

                        
          if Qi_Annealing > Qi_first % condition of fulfillment
              
             v=v_initial;
             s=s_initial;
             s_best=s_initial; % remember the best solution
             v_best=v_initial; % remember the best solution    
             
			 Anneling_Table_Best_parametersi={Qi_Annealing, v_best, s_best};
             Table_Best_parameters={Table_Best_parameters{1,1:end}, Anneling_Table_Best_parametersi{1,1:end}};

          else   % randomly take the worse consideration with a certain probability
              y= rand(1 ,1);
              if y < exp((-(Qi_Annealing-Qi_first))/T)
                 v=v_initial;
                 s=s_initial;
              end   
          end 
                        
          


         if Qi_Annealing > Qi_first, break ; end % we end the loop while when the average Qi result is satisfactory for us,
        end
        Qi_first = Qi_Annealing % new condition while
        T=a*T; %temperature cooling
        Era=Era+1 % number of solutions found with simulated annealing
 end



 

Table_Best_parameters=Table_Best_parameters';
[n , m]=size(Table_Best_parameters);

Table_Best_parameters = reshape(Table_Best_parameters,[3,n/3])';
Table_Best_parameters= sortrows(Table_Best_parameters, 1); %sorted table of the best solutions from different eras
v_best=Table_Best_parameters(end, 2);
v_best=v_best{1, 1};
s_best=Table_Best_parameters(end, 3);
s_best=s_best{1, 1};


%variable knowledge base  (vDB)
vDB_Qi=[];
vDB_tenderness=[];
cd 'reference data'
for i=1:50
data = [data_name int2str(i)];

reference_data_learning = append('daneU_',data_name);
reference_data_learning = load(data);
reference_data_learning = struct2cell(reference_data_learning);
reference_data_learning = reference_data_learning{2 , 1};

reference_data_Test = append('daneT_',data_name);
reference_data_Test = load(data);
reference_data_Test = struct2cell(reference_data_Test);
reference_data_Test = reference_data_Test{1 , 1};

Conclusion_vDB   = Conclusion(reference_data_learning , v_best, s_best, 1, nrule);
[out_vDB, specificity, tenderness]  = Classify(Conclusion_vDB, s_best, v_best, reference_data_Test, nrule, 0, 0, 0, 0);
vDB_Qi=[vDB_Qi, out_vDB];   
vDB_tenderness=[vDB_tenderness, tenderness];
end
QI_vDB=mean(vDB_Qi)
tenderness_vDB = mean(vDB_tenderness)


 vDB_Qi=vDB_Qi';
 [~,GG] = max(vDB_Qi(:,1));
 Max_vDB_Qi = vDB_Qi(GG,:);
 indexB=find(vDB_Qi == Max_vDB_Qi);
 indexB=indexB(1, 1); 

 
data= [data_name int2str(indexB)];
reference_data_learning = load(data);
reference_data_learning = struct2cell(reference_data_learning);
reference_data_learning = reference_data_learning{2 , 1};


Conclusion_BEST   = Conclusion(reference_data_learning , v_best, s_best, 1, nrule);


%permanent knowledge base (pDB, better results)

pDB_Qi=[];
pDB_tenderness=[];
for i=1:50
data= [data_name int2str(i)];

reference_data_Test =append('daneT_',data_name);
reference_data_Test = load(data);
reference_data_Test = struct2cell(reference_data_Test);
reference_data_Test = reference_data_Test{1 , 1};

[out_vDB, specificity, tenderness]  = Classify(Conclusion_BEST, s_best, v_best, reference_data_Test, nrule, 0, 0, 0, 0);
pDB_Qi=[pDB_Qi, out_vDB];   
pDB_tenderness=[pDB_tenderness, tenderness];
end
QI_pDB=mean(pDB_Qi)
tenderness_pDB = mean(pDB_tenderness)


cd(currentFolder)
timeElapsed = toc
