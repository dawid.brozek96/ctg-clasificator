

Chudacek_Percentyl = removevars(Chudacek,{'ID' 'TydzCiazy' 'Apgar_1' 'Ph' 'BE'});
Chudacek_Ph = removevars(Chudacek,{'ID' 'TydzCiazy' 'Apgar_1' 'Percentyl' 'BE'});
Chudacek_Apgar = removevars(Chudacek,{'ID' 'TydzCiazy' 'Percentyl' 'Ph' 'BE'});


Nazwy_Zmiennych_Percentyl = {'SredFHR' 'Falowanie' 'CzestSK' 'CzestAK' 'CzestDE' 'NiskaZmien' 'WskLTV' 'WskSTV' 'WskIloraz' 'AmplOsc' 'OscMil' 'OscSkacz' 'Percentyl'};
Nazwy_Zmiennych_Ph = {'SredFHR' 'Falowanie' 'CzestSK' 'CzestAK' 'CzestDE' 'NiskaZmien' 'WskLTV' 'WskSTV' 'WskIloraz' 'AmplOsc' 'OscMil' 'OscSkacz' 'Ph'};
Nazwy_Zmiennych_Apgar = {'SredFHR' 'Falowanie' 'CzestSK' 'CzestAK' 'CzestDE' 'NiskaZmien' 'WskLTV' 'WskSTV' 'WskIloraz' 'AmplOsc' 'OscMil' 'OscSkacz' 'Apgar_1'};


Chudacek_Percentyl_Flaga= Chudacek_Percentyl;
Chudacek_Percentyl_Flaga(:,:) = [];
Chudacek_Percentyl_Flaga_Plus= Chudacek_Percentyl;
Chudacek_Percentyl_Flaga_Plus(:,:) = [];
Chudacek_Percentyl_Flaga_Minus= Chudacek_Percentyl;
Chudacek_Percentyl_Flaga_Minus(:,:) = [];
%``````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````
%Normalizacja

dane_usun = table2array(Chudacek_Percentyl);
dane_usun_Ph = table2array(Chudacek_Ph);
dane_usun_Apgar = table2array(Chudacek_Apgar);

[n,m]=size(dane_usun);
mmin=min(dane_usun(:,1:m-1));
mmax=max(dane_usun(:,1:m-1));
dane_ZW_norm=zeros(n,m);

     for i=1:m-1
         minimum=mmin(1,i);
         maksimum=mmax(1,i);
         a=1/(maksimum-minimum);
         b=-a*minimum;
         dane_ZW_norm(:,i)=a*dane_usun(:,i)+b;
         dane_ZW_norm_Percentyl=dane_ZW_norm;
         dane_ZW_norm_Ph=dane_ZW_norm;
         dane_ZW_norm_Apgar=dane_ZW_norm;
     end
     
      dane_ZW_norm_Percentyl(:,m)=dane_usun(:,m);
      dane_ZW_norm_Ph(:,m)=dane_usun_Ph(:,m);
      dane_ZW_norm_Apgar(:,m)=dane_usun_Apgar(:,m);
      
      Chudacek_Percentyl=array2table(dane_ZW_norm_Percentyl);
      Chudacek_Percentyl.Properties.VariableNames = Nazwy_Zmiennych_Percentyl;
      
      Chudacek_Ph=array2table(dane_ZW_norm_Ph);
      Chudacek_Ph.Properties.VariableNames = Nazwy_Zmiennych_Ph;
      
      Chudacek_Apgar=array2table(dane_ZW_norm_Apgar);
      Chudacek_Apgar.Properties.VariableNames = Nazwy_Zmiennych_Apgar;

%````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````
%obliczenie flag

for i= 1 : height(Chudacek_Percentyl)
    vector_Percentyl = table2array(Chudacek_Percentyl(i:i,:));


    if (vector_Percentyl(length(vector_Percentyl))>5)
        vector_Percentyl([length(vector_Percentyl)]) = [1];
        vector_Percentyl  = array2table(vector_Percentyl);
        vector_Percentyl.Properties.VariableNames = Nazwy_Zmiennych_Percentyl;
        Chudacek_Percentyl_Flaga = [Chudacek_Percentyl_Flaga; vector_Percentyl]; 
        
        Chudacek_Percentyl_Flaga_Plus = [Chudacek_Percentyl_Flaga_Plus; vector_Percentyl];
        
    else
        vector_Percentyl([length(vector_Percentyl)]) = [-1];
        vector_Percentyl  = array2table(vector_Percentyl);
        vector_Percentyl.Properties.VariableNames = Nazwy_Zmiennych_Percentyl;
        Chudacek_Percentyl_Flaga = [Chudacek_Percentyl_Flaga; vector_Percentyl];
        
        Chudacek_Percentyl_Flaga_Minus = [Chudacek_Percentyl_Flaga_Minus; vector_Percentyl];
        
    end
end




Chudacek_Ph_Flaga= Chudacek_Ph;
Chudacek_Ph_Flaga(:,:) = [];
Chudacek_Ph_Flaga_Plus= Chudacek_Ph;
Chudacek_Ph_Flaga_Plus(:,:) = [];
Chudacek_Ph_Flaga_Minus= Chudacek_Ph;
Chudacek_Ph_Flaga_Minus(:,:) = [];

for i= 1 : height(Chudacek_Ph)
    vector_Ph = table2array(Chudacek_Ph(i:i,:));


    if (vector_Ph(length(vector_Ph))>= 7.1)
        vector_Ph([length(vector_Ph)]) = [1];
        vector_Ph  = array2table(vector_Ph);
        vector_Ph.Properties.VariableNames = Nazwy_Zmiennych_Ph;
        Chudacek_Ph_Flaga = [Chudacek_Ph_Flaga; vector_Ph];
        
        Chudacek_Ph_Flaga_Plus = [Chudacek_Ph_Flaga_Plus; vector_Ph];
        
    else
        vector_Ph([length(vector_Ph)]) = [-1];
        vector_Ph  = array2table(vector_Ph);
        vector_Ph.Properties.VariableNames = Nazwy_Zmiennych_Ph;
        Chudacek_Ph_Flaga = [Chudacek_Ph_Flaga; vector_Ph];
        
        Chudacek_Ph_Flaga_Minus = [Chudacek_Ph_Flaga_Minus; vector_Ph];
        
    end
end




Chudacek_Apgar_Flaga= Chudacek_Apgar;
Chudacek_Apgar_Flaga(:,:) = [];
Chudacek_Apgar_Flaga_Plus= Chudacek_Apgar;
Chudacek_Apgar_Flaga_Plus(:,:) = [];
Chudacek_Apgar_Flaga_Minus= Chudacek_Apgar;
Chudacek_Apgar_Flaga_Minus(:,:) = [];

for i= 1 : height(Chudacek_Apgar)
    vector_Apgar = table2array(Chudacek_Apgar(i:i,:));


    if (vector_Apgar(length(vector_Apgar)) > 4)
        vector_Apgar([length(vector_Apgar)]) = [1];
        vector_Apgar  = array2table(vector_Apgar);
        vector_Apgar.Properties.VariableNames = Nazwy_Zmiennych_Apgar;
        Chudacek_Apgar_Flaga = [Chudacek_Apgar_Flaga; vector_Apgar];
        
        Chudacek_Apgar_Flaga_Plus = [Chudacek_Apgar_Flaga_Plus; vector_Apgar];
        
    else
        vector_Apgar([length(vector_Apgar)]) = [-1];
        vector_Apgar  = array2table(vector_Apgar);
        vector_Apgar.Properties.VariableNames = Nazwy_Zmiennych_Apgar;
        Chudacek_Apgar_Flaga = [Chudacek_Apgar_Flaga; vector_Apgar];
        
        Chudacek_Apgar_Flaga_Minus = [Chudacek_Apgar_Flaga_Minus; vector_Apgar];
        
    end
end

%````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````
%Podzia� na dane uczace sie i testowe




for j = 1:50
    %Percentyl
    Test_Percentyl_Minus=Chudacek_Percentyl_Flaga_Minus; 
    index_Percentyl_Minus=randsample(1:height(Chudacek_Percentyl_Flaga_Minus),floor((height(Chudacek_Percentyl_Flaga_Minus)/2))) ;
    Uczacy_Percentyl_Minus = Test_Percentyl_Minus(index_Percentyl_Minus,:) ; % pick rows randomly 
    Test_Percentyl_Minus(index_Percentyl_Minus,:)=[]; % remove those rows
    %Percentyl_TM{j} = Test_Percentyl_Minus;
    %Percentyl_UM{j} = Uczacy_Percentyl_Minus;

    Test_Percentyl_Plus=Chudacek_Percentyl_Flaga_Plus; 
    index_Percentyl_Plus=randsample(1:height(Chudacek_Percentyl_Flaga_Plus),floor((height(Chudacek_Percentyl_Flaga_Plus)/2))) ;
    Uczacy_Percentyl_Plus = Test_Percentyl_Plus(index_Percentyl_Plus,:) ; % pick rows randomly 
    Test_Percentyl_Plus(index_Percentyl_Plus,:)=[]; % remove those rows
    %Percentyl_TP{j} = Test_Percentyl_Plus;
    %Percentyl_UP{j} = Uczacy_Percentyl_Plus;
    
      


    %Apgar
    Test_Apgar_Minus=Chudacek_Apgar_Flaga_Minus; 
    index_Apgar_Minus=randsample(1:height(Chudacek_Apgar_Flaga_Minus),floor((height(Chudacek_Apgar_Flaga_Minus)/2))) ;
    Uczacy_Apgar_Minus = Test_Apgar_Minus(index_Apgar_Minus,:) ; % pick rows randomly 
    Test_Apgar_Minus(index_Apgar_Minus,:)=[]; % remove those rows
    %Apgar_TM{j} = Test_Apgar_Minus;
    %Apgar_UM{j} = Uczacy_Apgar_Minus;

    Test_Apgar_Plus=Chudacek_Apgar_Flaga_Plus; 
    index_Apgar_Plus=randsample(1:height(Chudacek_Apgar_Flaga_Plus),floor((height(Chudacek_Apgar_Flaga_Plus)/2))) ;
    Uczacy_Apgar_Plus = Test_Apgar_Plus(index_Apgar_Plus,:) ; % pick rows randomly 
    Test_Apgar_Plus(index_Apgar_Plus,:)=[]; % remove those rows
    %Apgar_TP{j} = Test_Apgar_Plus;
    %Apgar_UP{j} = Uczacy_Apgar_Plus;
    
    Zbior_uczacy_Apgar{j} = [Uczacy_Apgar_Plus ;Uczacy_Apgar_Minus];
    Zbior_testowy_Apgar{j} = [Test_Apgar_Plus ;Test_Apgar_Minus];

%Ph
    Test_Ph_Minus=Chudacek_Ph_Flaga_Minus; 
    index_Ph_Minus=randsample(1:height(Chudacek_Ph_Flaga_Minus),floor((height(Chudacek_Ph_Flaga_Minus)/2))) ;
    Uczacy_Ph_Minus = Test_Ph_Minus(index_Ph_Minus,:) ; % pick rows randomly 
    Test_Ph_Minus(index_Ph_Minus,:)=[]; % remove those rows
    %Ph_TM{j} = Test_Ph_Minus;
    %Ph_UM{j} = Uczacy_Ph_Minus;
    

    Test_Ph_Plus=Chudacek_Ph_Flaga_Plus; 
    index_Ph_Plus=randsample(1:height(Chudacek_Ph_Flaga_Plus),floor((height(Chudacek_Ph_Flaga_Plus)/2))) ;
    Uczacy_Ph_Plus = Test_Ph_Plus(index_Ph_Plus,:) ; % pick rows randomly 
    Test_Ph_Plus(index_Ph_Plus,:)=[]; % remove those rows
    %Ph_TP{j} = Test_Ph_Plus;
    %Ph_UP{j} = Uczacy_Ph_Plus;
    
    Zbior_uczacy_Ph{j} = [Uczacy_Ph_Plus ;Uczacy_Ph_Minus];
    Zbior_testowy_Ph{j} = [Test_Ph_Plus ;Test_Ph_Minus]; 
    
    %``````````````````````````````````````````````````````````````````````````````````````````````````````````````````
    %zapisanie do pliku
    
    
    Zbior_uczacy_Percentyl{j} = [Uczacy_Percentyl_Plus ;Uczacy_Percentyl_Minus];
    Zbior_testowy_Percentyl{j} = [Test_Percentyl_Plus ;Test_Percentyl_Minus];
    Zbior_uczacy_Percentyl{j}=Zbior_uczacy_Percentyl{j}(randperm(275)',:);
    Zbior_testowy_Percentyl{j}=Zbior_testowy_Percentyl{j}(randperm(276)',:);
      
    M_Zbior_uczacy_Percentyl{j} = table2array(Zbior_uczacy_Percentyl{j});
    M_Zbior_testowy_Percentyl{j} = table2array(Zbior_testowy_Percentyl{j});
    daneU_Percentyl= M_Zbior_uczacy_Percentyl{j};
    daneT_Percentyl= M_Zbior_testowy_Percentyl{j}; 
    nazwa=['Percentyl' int2str(j)];
    save(nazwa,'daneU_Percentyl','daneT_Percentyl');
    
    
    Zbior_uczacy_Ph{j} = [Uczacy_Ph_Plus ;Uczacy_Ph_Minus];
    Zbior_testowy_Ph{j} = [Test_Ph_Plus ;Test_Ph_Minus];
    Zbior_uczacy_Ph{j}=Zbior_uczacy_Ph{j}(randperm(275)',:);
    Zbior_testowy_Ph{j}=Zbior_testowy_Ph{j}(randperm(276)',:);
    
    M_Zbior_uczacy_Ph{j} = table2array(Zbior_uczacy_Ph{j});
    M_Zbior_testowy_Ph{j} = table2array(Zbior_testowy_Ph{j});
    daneU_Ph= M_Zbior_uczacy_Ph{j};
    daneT_Ph= M_Zbior_testowy_Ph{j}; 
    nazwa=['Ph' int2str(j)];
    save(nazwa,'daneU_Ph','daneT_Ph');
    
    
    Zbior_uczacy_Apgar{j} = [Uczacy_Apgar_Plus ;Uczacy_Apgar_Minus];
    Zbior_testowy_Apgar{j} = [Test_Apgar_Plus ;Test_Apgar_Minus];
    Zbior_uczacy_Apgar{j}=Zbior_uczacy_Apgar{j}(randperm(275)',:);
    Zbior_testowy_Apgar{j}=Zbior_testowy_Apgar{j}(randperm(276)',:);
    
    
    M_Zbior_uczacy_Apgar{j} = table2array(Zbior_uczacy_Apgar{j});
    M_Zbior_testowy_Apgar{j} = table2array(Zbior_testowy_Apgar{j});
    daneU_Apgar= M_Zbior_uczacy_Apgar{j};
    daneT_Apgar= M_Zbior_testowy_Apgar{j}; 
    nazwa=['Apgar' int2str(j)];
    save(nazwa,'daneU_Apgar','daneT_Apgar');
    
    
    
    
    
end


mergedU = M_Zbior_uczacy_Ph{1};
for k = 2:5
    mergedU = [mergedU ;M_Zbior_uczacy_Ph{k}];

end
save('mergedU_Ph.mat', 'mergedU');
mergedT = M_Zbior_testowy_Ph{1};
for k = 2:5
    mergedT = [mergedT ;M_Zbior_testowy_Ph{k}];

end
save('mergedT_Ph.mat', 'mergedT');



mergedU = M_Zbior_uczacy_Percentyl{1};
for k = 2:5
    mergedU = [mergedU ;M_Zbior_uczacy_Percentyl{k}];

end
save('mergedU_Percentyl.mat', 'mergedU');
mergedT = M_Zbior_testowy_Percentyl{1};
for k = 2:5
    mergedT = [mergedT ;M_Zbior_testowy_Percentyl{k}];

end
save('mergedT_Percentyl.mat', 'mergedT');






mergedU = M_Zbior_uczacy_Apgar{1};
for k = 2:5
    mergedU = [mergedU ;M_Zbior_uczacy_Apgar{k}];

end
save('mergedU_Apgar.mat', 'mergedU');
mergedT = M_Zbior_testowy_Apgar{1};
for k = 2:5
    mergedT = [mergedT ;M_Zbior_testowy_Apgar{k}];

end
save('mergedT_Apgar.mat', 'mergedT');

