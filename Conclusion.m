function  P_daszek   = RMNK( data ,c , s , miter , nrule)

[n1 m1]=size(data);
tabela_klasyfiaktora=[];
%for j=1:miter
G=10E6* eye(m1*nrule); %inicjowanie macierzy G 10.23
P_daszek=zeros(m1*nrule, 1); %inicjonowanie, wszystkie parametry okreslajace polozenie zbiorow w konkluzjach regul 10.23
%p=zeros(m1 ,nrule); %incionowanie, parametry okreslajace polozenie zbioru w konkluzji
for n=1:n1    
    %F(x_0(k)) = exp(-0.5 * sum((x_0 - c)/(s)^2)); % 10.5 -> lepiej uzyc 10.6 bo nie musimy sie martwic o N
    diff1=(data(n,1:m1-1)'*ones(1 ,nrule))-c ; %10.5, i - liczba regul, zmienniamy recznie
    F=exp(-0.5*sum((diff1./s).^2)); %10.5 str 483
        if (sum(F) < eps )
        F=ones(1, nrule) * eps;
        end
    F_daszek= F / sum(F); % 10.17 str 490
    %tutaj zosta� dodany warunek, ze jesli suma stopnia aktywacji (sumF)
    %bedzie mniejszy od epsylona to wtedy wszystkie stopnie aktywacje
    %ustawia na epsylona 
    
    %TEST--------- 
    %F=ones(1 , 10) * 2.2204e-20; %eps = 2.2204e-16
    %Koniec testu--------
    

    
    D1=[data(n,1:m1-1)' ; 1] *F_daszek; %10.18 str 490
    D=D1(:); %10.18 - > wektor 130x1

    G = G - ((G*D*D'*G)/(D'*G*D+1));  %10.22 str491
    P_daszek= P_daszek +G*D*(data(n,m1)-D'*P_daszek); % wzor 10.21 str491, oszacowany wektor P uzyskany w k+1 interacji, p_daszek=pk 
    % rozmiar pasuje ze  wzorem 10.19, 130x1
%           if (n==n1)
%               p(:)=P_daszek;   %10.19 modyfikacja, do czego to potrzebne?  13x10 
%           end  
      %yi=(data(n1,1:m1-1) * p(1:m1 -1,:))+p(m1 ,:); %wnioski wyznaczone na  podstawie regul 10.8 jak?
      %y1=(yi*F')/sum(F); %10.13 + wziasc pod uwage epsylona -> PYTANIE: skoro w linijce 44-46 doda�em warunek to chyba nie musze go tutaj powtarzac?
%      y12=(sum(yi*F'))/sum(F); 
      %y2 =sum(F * p' * data(n , :)')/sum(F); % 10.13 proba 2.0
      %y22 =(F * p' * data(n , :)')/sum(F); % 10.13 proba 2.1
      %y3 =sum(F * p(1, 1:nrule)' * data(n , :))/sum(F) ; % 10.13 proba 3.0
      %y32 =(F * p(1, 1:nrule)' * data(n , :))/sum(F) ; % 10.13 proba 3.1
%       y_k1=yi*F'; -> liczba wiec nie musimy liczyc sumy z linijki 59
%       y_k2=p' * data(n , :)'; %wektor 10x1
%       y_k22 =(F * p' * data(n , :)'); %liczba, wiec y_k22 = y_k23
%       y_k23 =sum(F * p' * data(n , :)');
%       y_k3=p(1, 1:nrule)' * data(n , :); %wektor 10x13
%       y_k32 =(F * p(1, 1:nrule)' * data(n , :)); %wektor 1x13
%       y_k33 =sum(F * p(1, 1:nrule)' * data(n , :)); %liczba
      
      %sprawdzenie
      %y_spr2=D' * P_daszek; % 10.20, dlaczego musze transponowac macierz??
      %y_spr2=sum(D .* P_daszek); % sumuje po wszystkich obrazach 1:276 y_spr1=y_spr2
      %tabela_klasyfiaktora = [tabela_klasyfiaktora ; y_spr2];
      %wzor na rozmair macierzy (n x m) X (m x k) = (n x k)
      
     end
%end
end



