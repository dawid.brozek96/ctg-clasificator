A program designed to determine (classify) the expected health of the fetus on the basis of previously
developed quantitative parameters obtained through CTG tests. The classifier was based on the existing one
already adaptive neuro-fuzzy inference system (ANFIS). In the original ANFIS system, a modified gradient method was used to determine the parameters of the premises, but here it was modified by implementing the simulated annealing algorithm.
The recursive least squares method was used to calculate the conclusion parameters.
To check the quality of the designed class-factor, was used the "quality index" (QI).